
# JOYET Hugo - Examen Cybersécurité OS

## Vulnérabilitées

1. [LM Hash activé](#1---lm-hash-activé)
2. [Utilisation de NTLMv1](#2---utilisation-de-ntlmv1)
3. [Mot de passe en clair](#3---mot-de-passe-en-clair)
4. [Stratégie de verrouillage de compte](#4---stratégie-de-verrouillage-de-compte)
5. [Bitlocker désactivé](#5---bitlocker-désactivé)
6. [Tous les droits dans le dossier Administrateur](#6---tous-les-droits-dans-le-dossier-administrateur)
7. [L'utilisateur a tous les droits sur service.exe](#7---lutilisateur-a-tous-les-droits-sur-serviceexe)
8. [Toutes les applications installées tournent avec les droits Administrateur](#8---toutes-les-applications-installées-tournent-avec-les-droits-administrateur)


### 1 - LM Hash activé

![](./screenshots/LMHASH.png)


Le LM Hash (Lan Manager Hash) est une méthode de hachage obsolète utilisée par les anciennes versions du système d'exploitation Windows pour stocker les mots de passe des utilisateurs. Cependant, en raison de sa vulnérabilité, son utilisation est fortement déconseillée. Voici quelques risques associés à l'utilisation de LM Hash :

1. Vulnérabilité aux attaques par force brute : Étant donné que le LM Hash divise les mots de passe en deux parties plus petites, il est plus facile à casser à l'aide de techniques de force brute, ce qui rend les comptes utilisateurs vulnér ables aux attaques.

2. Exposition des mots de passe : Les LM Hash sont moins sécurisés que les méthodes de hachage modernes, ce qui signifie que les mots de passe des utilisateurs peuvent être exposés en cas de compromission du système.

3. Risque de compromission des comptes : Les attaquants peuvent utiliser des outils facilement disponibles pour casser les LM Hash et accéder aux comptes utilisateur, compromettant ainsi la sécurité globale du réseau.

#### 1.1 - Recommandations

Pour atténuer ces risques liés à l'utilisation de LM Hash, voici quelques recommandations :

1. Désactivation du stockage LM Hash : Assurez-vous que le stockage LM Hash est désactivé sur tous les systèmes Windows. Cette action est essentielle pour renforcer la sécurité des mots de passe des utilisateurs.

2. Utilisation de méthodes de hachage sécurisées : Encouragez l'utilisation de méthodes de hachage plus sécurisées telles que NTLM (NT LAN Manager) ou des protocoles d'authentification plus récents pour le stockage des mots de passe.

En suivant ces recommandations, vous pouvez renforcer la sécurité des mots de passe des utilisateurs et réduire les risques liés à l'utilisation de LM Hash.

### 2 - Utilisation de NTLMv1

L'utilisation de NTLMv1 (NT LAN Manager version 1) présente des risques pour la sécurité, car cette méthode d'authentification est considérée comme obsolète et vulnérable aux attaques. Voici quelques risques associés à l'utilisation de NTLMv1 :

1. Vulnérabilité aux attaques par interception : NTLMv1 est vulnérable aux attaques de type "rejeu" où les informations d'identification peuvent être interceptées et réutilisées pour accéder au réseau.

2. Faible résistance aux attaques par force brute : Étant donné que NTLMv1 ne prend pas en charge les algorithmes de hachage plus sécurisés, il est plus facile à casser à l'aide de techniques de force brute, mettant ainsi en péril la sécurité des comptes utilisateurs.

3. Risque de compromission des informations d'identification : Les attaquants peuvent exploiter les vulnérabilités de NTLMv1 pour compromettre les informations d'identification des utilisateurs et accéder aux ressources sensibles du réseau.

![](./screenshots/reponse_lm_ntlm.png)

#### 2.1 - Recommandations

Pour résoudre cette vulnérabilité et renforcer la sécurité de votre système, voici quelques recommandations :

1. Désactivation de NTLMv1 : Assurez-vous que NTLMv1 est désactivé sur tous les systèmes et que seules les versions plus sécurisées de NTLM (comme NTLMv2) sont autorisées.

2. Mise à jour des protocoles d'authentification : Encouragez l'utilisation de protocoles d'authentification plus robustes et sécurisés tels que NTLMv2, Kerberos, ou mieux encore, l'authentification à base de certificats.


### 3 - Mot de passe en clair

![](./screenshots/passwd_clair.png)

Le stockage du mot de passe administrateur en texte clair dans un fichier présente un risque majeur en matière de sécurité, car cela expose directement les informations d'identification sensibles à toute personne ayant accès au fichier. Voici quelques dangers associés à cette pratique :

1. Exposition des informations d'identification : Le fait de stocker des mots de passe en clair les rend facilement accessibles aux utilisateurs non autorisés, ce qui compromet la sécurité du compte administrateur et expose le système à des risques de violation.

2. Risque de compromission du système : Si un attaquant parvient à accéder au fichier contenant le mot de passe administrateur en clair, il peut obtenir un accès non autorisé au système, ce qui pourrait entraîner des conséquences graves telles que la perte de données sensibles ou des activités malveillantes.

3. Non-conformité aux meilleures pratiques de sécurité : Stocker des mots de passe en texte clair va à l'encontre des normes de sécurité recommandées, ce qui peut entraîner des problèmes de conformité et potentiellement des sanctions réglementaires.

#### 3.1 - Recommandations

Pour remédier à cette vulnérabilité et renforcer la sécurité de votre système, voici quelques recommandations :

1. Chiffrement des mots de passe : Utilisez des méthodes de chiffrement robustes pour stocker les mots de passe de manière sécurisée, de sorte qu'ils ne soient pas directement lisibles en cas d'accès non autorisé.

2. Gestion sécurisée des informations d'identification : Utilisez des outils de gestion des informations d'identification sécurisés tels que des coffres-forts de mots de passe ou des gestionnaires d'identités pour stocker et gérer les mots de passe de manière sécurisée.

3. Restriction des autorisations d'accès : Limitez l'accès aux fichiers contenant des informations d'identification sensibles uniquement aux utilisateurs autorisés ayant besoin d'y accéder, en appliquant des contrôles d'accès stricts.

4. Sensibilisation à la sécurité : Sensibilisez les membres de votre équipe sur les bonnes pratiques en matière de gestion des mots de passe et les risques associés au stockage de mots de passe en clair dans des fichiers non sécurisés.


En suivant ces recommandations, vous pouvez renforcer la sécurité de votre système en adoptant des pratiques de stockage des mots de passe plus sécurisées et en sensibilisant les utilisateurs sur l'importance de protéger les informations d'identification sensibles.


### 4 - Stratégie de verrouillage de compte

![](./screenshots/strategie_verrouillage.png)

L'absence d'une stratégie de verrouillage des comptes peut exposer votre système à des risques de sécurité importants. Sans une telle stratégie, les comptes utilisateurs peuvent être vulnérables à des attaques de force brute ou à des tentatives d'accès non autorisées répétées. Voici quelques risques liés à l'absence d'une stratégie de verrouillage des comptes :

1. Vulnérabilité aux attaques par force brute : En l'absence de verrouillage des comptes, les attaquants peuvent tenter de deviner les mots de passe en effectuant des attaques par force brute sans rencontrer de restrictions, ce qui augmente considérablement le risque de compromission des comptes.
    
2. Exposition aux activités malveillantes : Les comptes non verrouillés peuvent être exploités par des individus malveillants pour accéder illégalement au système, causer des dommages, ou accéder à des informations sensibles.
    
3. Non-conformité aux meilleures pratiques de sécurité : L'absence d'une stratégie de verrouillage des comptes contredit les normes de sécurité recommandées, ce qui peut conduire à des problèmes de conformité et à des risques juridiques.


### 5 - Bitlocker désactivé

Ici nous voyons grâce à l'outil Hardening Kitty que Bitlocker est désactivé sur le poste et donc le disque dur n'est pas chiffré.

![](./screenshots/bitlocker_off.png)

Lorsque BitLocker est désactivé sur un poste de travail ou un ordinateur portable, cela présente plusieurs dangers potentiels en matière de sécurité. Voici quelques risques liés à la désactivation de BitLocker :

1. Exposition des données sensibles : Sans le chiffrement offert par BitLocker, les données sensibles stockées sur le disque dur de l'ordinateur deviennent vulnérables en cas de perte ou de vol de l'appareil.

2. Risque de violation de la confidentialité : Les données personnelles et professionnelles peuvent être exposées en cas de vol ou d'accès non autorisé à l'ordinateur.

3. Menaces de logiciels malveillants : En l'absence de BitLocker, les logiciels malveillants ont un accès plus facile aux données stockées sur le disque dur, ce qui peut conduire à la compromission du système.

#### 5.1 - Recommandations

Activer BitLocker : Assurez-vous d'activer BitLocker pour tous les disques durs pertinents sur le poste de travail. Cela permettra de chiffrer les données et d'offrir une protection en cas de perte ou de vol de l'appareil.

### 6 - Tous les droits dans le dossier Administrateur

Avec l'outil WinPeas :

![](./screenshots/all_access_admin.png)

Accorder à un utilisateur tous les droits dans le dossier `C:\Users\Administrateur` peut représenter un risque sérieux pour la sécurité de votre système, car cela permet à l'utilisateur de modifier, supprimer ou accéder à des fichiers sensibles, y compris des données critiques du système. Cela peut également rendre le système vulnérable à des actions malveillantes ou à des modifications non autorisées. Voici quelques risques liés à l'octroi de tous les droits à un utilisateur dans ce dossier :

1. Risque de modification ou de suppression non autorisée : L'utilisateur dispose de tous les droits, ce qui lui permet de modifier, supprimer ou déplacer des fichiers critiques dans le dossier `C:\Users\Administrateur`, ce qui peut entraîner des dysfonctionnements du système ou la perte de données importantes.

2. Vulnérabilité aux logiciels malveillants : Lorsqu'un utilisateur dispose de tous les droits, les logiciels malveillants peuvent facilement accéder à ce dossier et compromettre la sécurité du système, en installant des logiciels indésirables ou en exécutant des scripts nuisibles.

3. Exposition des informations sensibles : L'accès total au dossier peut exposer des informations sensibles ou confidentielles qui peuvent compromettre la sécurité et la confidentialité des données.

#### 6.1 - Recommandations

Pour atténuer ces risques et renforcer la sécurité de votre système, voici quelques recommandations :

1. Restriction des droits d'accès : Limitez les droits d'accès des utilisateurs uniquement aux fichiers et aux dossiers essentiels dont ils ont besoin pour leurs tâches spécifiques. Évitez d'accorder des droits d'accès complets sans nécessité absolue.

2. Application de la séparation des privilèges : Mettez en œuvre le principe de moindre privilège en accordant uniquement les autorisations requises pour effectuer les tâches spécifiques, afin de limiter l'exposition aux risques potentiels.

3. Surveillance des activités : Utilisez des outils de surveillance des activités pour détecter les activités suspectes ou les modifications non autorisées dans le dossier `C:\Users\Administrateur`, ce qui permettra de prendre des mesures appropriées en cas de comportements malveillants.

En suivant ces recommandations, vous pourrez réduire les risques associés à l'octroi de tous les droits à un utilisateur dans le dossier `C:\Users\Administrateur` et renforcer la sécurité de votre système en limitant l'accès aux fichiers sensibles.

### 7 - L'utilisateur a tous les droits sur service.exe

Avec l'outil WinPeas, on voit qu l'utilisateur a tous les droits sur `C:\Program Files (x86)\Contoso Services\Contoso Hello\service.exe`

![](./screenshots/serviceexe_all_access.png)

Lorsqu'un utilisateur détient tous les droits sur un fichier exécutable spécifique à l'entreprise, cela peut entraîner des risques considérables pour la sécurité et la confidentialité des données de l'entreprise. Cette situation peut faciliter les modifications non autorisées, l'exécution de codes malveillants ou l'accès à des fonctionnalités sensibles du programme. Voici quelques risques associés à cette configuration :

1. Altération non autorisée du programme : L'utilisateur disposant de tous les droits peut potentiellement altérer le fonctionnement du programme, introduire des bugs ou des vulnérabilités de sécurité, ce qui peut compromettre l'intégrité des opérations de l'entreprise.

2. Exécution de codes malveillants : Si un utilisateur malveillant obtient un accès non autorisé au service.exe, il peut y injecter des codes malveillants, ce qui peut entraîner des conséquences graves telles que des attaques de logiciels malveillants ou des vols de données.

![](./screenshots/icacls_serviceexe.png)

3. Fuites de données sensibles : L'accès complet au fichier exécutable peut potentiellement conduire à des fuites de données sensibles ou confidentielles de l'entreprise, ce qui peut avoir des répercussions néfastes sur la réputation et la sécurité globale de l'entreprise.

#### 7.1 - Recommandations

Pour atténuer ces risques et renforcer la sécurité de l'entreprise, voici quelques recommandations :

1. Limitation des privilèges : Limitez les droits d'accès pour l'utilisateur à seulement ceux nécessaires pour effectuer des tâches spécifiques, afin de réduire les risques d'altération non autorisée ou d'accès malveillant.

2. Surveillance des activités : Mettez en place un système de surveillance pour détecter les activités suspectes ou les modifications non autorisées sur le service.exe, afin de pouvoir prendre des mesures préventives en cas de comportements malveillants.

### 8 - Toutes les applications installées tournent avec les droits Administrateur

Avec l'outil WinPeas nous regardons si les applications installées sont exécutées avec les droits Administrateur

![](./screenshots/alwaysinstalledelevated.png)

L'exécution de toutes les applications installées avec des droits administrateurs représente un risque sérieux pour la sécurité de votre système. Cela ouvre la porte à des menaces potentielles, telles que l'exécution de logiciels malveillants, la compromission des données sensibles, et des atteintes à la confidentialité. Voici quelques risques associés à cette pratique :

1. Exposition aux logiciels malveillants : L'exécution d'applications avec des privilèges élevés augmente la vulnérabilité du système aux logiciels malveillants et aux attaques ciblées, car ces applications ont un accès étendu qui peut être exploité par des acteurs malveillants.

2. Vulnérabilité aux atteintes à la sécurité : Les applications exécutées avec des droits administrateurs sont plus susceptibles de compromettre la sécurité du système en raison de la possibilité d'apporter des modifications non autorisées aux fichiers système et aux paramètres de sécurité.

3. Risque de dommages au système : L'exécution d'applications avec des privilèges élevés accroît le risque de dommages accidentels au système, ce qui peut entraîner des dysfonctionnements critiques ou des pertes de données irrécupérables.

#### 8.1 - Recommandations

Pour atténuer ces risques et renforcer la sécurité de votre système, voici quelques recommandations :

1. Utilisation de comptes d'utilisateur standard : Encouragez l'utilisation de comptes d'utilisateur standard pour l'exécution quotidienne des applications, réservant les droits administrateurs uniquement aux tâches qui le nécessitent.

2. Mise en œuvre du principe de moindre privilège : Appliquez le principe de moindre privilège en attribuant aux utilisateurs uniquement les droits et les privilèges nécessaires à l'exécution de leurs tâches spécifiques, ce qui limite les risques potentiels.

## Conclusion

En somme, votre système présente plusieurs vulnérabilités qui pourraient compromettre sérieusement sa sécurité. Les risques identifiés comprennent la désactivation de BitLocker, l'utilisation de LM Hash et NTLMv1, le stockage de mots de passe en texte clair, l'absence d'une stratégie de verrouillage des comptes, les droits excessifs accordés à l'utilisateur dans des dossiers sensibles, ainsi que l'exécution de toutes les applications avec des droits administrateurs.

Ces vulnérabilités peuvent exposer votre système à un large éventail de menaces, allant de l'accès non autorisé et la compromission des données à l'exécution de logiciels malveillants et aux activités d'attaquants malveillants. Elles pourraient également entraîner des problèmes de conformité et de confidentialité des données, ce qui pourrait avoir des conséquences graves sur votre entreprise.

Pour remédier à ces vulnérabilités et renforcer la sécurité de votre système, il est impératif de prendre des mesures immédiates. Cela comprend l'activation de BitLocker, la mise à niveau vers des méthodes de hachage plus sécurisées, la mise en place d'une politique de verrouillage des comptes, la sécurisation du stockage des mots de passe, la limitation des droits d'accès, la mise en place de privilèges d'utilisateur appropriés, ainsi que la sensibilisation et la formation des utilisateurs sur les meilleures pratiques en matière de sécurité des données.

En suivant ces recommandations et en mettant en place des mesures de sécurité appropriées, vous pourrez réduire les risques de sécurité et renforcer la robustesse de votre système contre les menaces potentielles, assurant ainsi la protection de vos données sensibles et le bon fonctionnement de votre entreprise.